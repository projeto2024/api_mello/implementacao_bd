const router = require("express").Router();
const dbController = require('../controller/dbController');
const clienteController = require('../controller/clienteController')
const VooController = require('../controller/vooController');
const ReservaController = require('../controller/reservaController');

//cliente
router.post('/clientePost/', clienteController.postUsuario);
router.get('/clienteLista/', clienteController.getListas);
router.put('/clienteAtualizar/', clienteController.updateUsuario);
router.delete('/deleteCliente/', clienteController.deleteUsuarios);
router.post('/clienteLogin/', clienteController.loginUsuarios);

//voo
router.post('/voo', VooController.criarVoo);
router.get('/voo', VooController.listarVoos);
router.put('/voo/:Numero', VooController.atualizarVoo);
router.delete('/voo/:Numero', VooController.deletarVoo);

//reserva
router.post('/reserva', ReservaController.criarReserva);
router.get('/reserva', ReservaController.listarReservas);
router.put('/reserva/:Reserva_ID', ReservaController.atualizarReserva);
router.delete('/reserva/:Reserva_ID', ReservaController.deletarReserva);

// Rota para consultar todas as tabelas e suas descrições
router.get("/tabelas", dbController.getTable);

// Rota para consultar somente os nomes das tabelas
router.get("/nomes", dbController.getTableNames);

//rota para select da tabela cliente
router.get("/clientes/", clienteController.getAllClientes);

//rota para select da tabela cliente com filtro
router.get("/clientes2/", clienteController.getAllClientes2)

module.exports = router;
