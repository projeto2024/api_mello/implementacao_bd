const { connection } = require('../db/connect');

module.exports = class clienteController {

    static async postUsuario(req, res) {
        const { nome, email, telefone, password } = req.body;
        if (!nome || !email || !telefone || !password) {
          res.status(400).json({ message: "Preencha todos os campos obrigatórios." });
        } else {
          try {
            // Adicione aqui a lógica para verificar se o e-mail já está em uso
            res.status(201).json({ message: "Usuário cadastrado com sucesso." });
          } catch (error) {
            console.error('Erro ao cadastrar usuário:', error);
            res.status(500).json({ message: "Erro ao cadastrar usuário." });
          }
        }
      }
  
    static async getListas(req, res) {
      //para listar 
      const listaUsuarios = [
            {
            nome: "a",
            email: "a",
            telefone: "a",
            password: "a",
            },
      ];
    
      // verifica se os campos estão vazios 
      listaUsuarios.forEach(user => {
        if (user.nome === "" || user.email === "" || user.telefone === "" || user.password === "") {
          res.status(500).json({ message: "Erro ao buscar lista de usuários." });
          return; 
        }
      });
    
      res.status(200).json({ usuarios: listaUsuarios });
    }
    
    // atualização de usuário
    static async updateUsuario(req, res) {
      const { nome, email, telefone, password } = req.body;
      if (nome === "" || email === "" || telefone === "" || password === "") {
        return res.status(200).json({ message: "Preencha os campos corretamente." });
      } else {
        return res.status(200).json({ message: "Usuário atualizado com sucesso." });
      }
    }
  
    // deleção de usuário
  static async deleteUsuarios(req, res) {
    const userId = req.body.id; // Obtendo o ID do usuário do corpo da requisição
    // Simulando um banco de dados de usuários
    let users = [
      { id: 1, nome: 'user1', email: 'email1', telefone: 'telefone1', password: '123' },
      { id: 2, nome: 'user2', email: 'email2', telefone: 'telefone2', password: '456' }
    ];
    // Encontrando o usuário pelo ID e removendo-o do array
    const index = users.findIndex(user => user.id === parseInt(userId));
    if (index !== -1) {
      users.splice(index, 1);
      // Enviando resposta de sucesso após a deleção do usuário
      res.status(200).json({ message: "Usuário removido com sucesso!" });
    } else {
      // Enviando resposta de usuário não encontrado
      res.status(404).json({ message: "Usuário não encontrado." });
    }
  }
  
  
   // login de usuário
  static async loginUsuarios(req, res) {
    const { email, password } = req.body; // Incluído cpf na desestruturação
    // Simulando um banco de dados de usuários
    const users = [
      { nome: 'user1', email: 'email1', telefone: 'telefone1', password: '123' }
    ];
    const foundUser = users.find(u => u.email === email && u.password === password);
    if (foundUser) {
      res.status(200).json({ message: 'Login bem-sucedido!' });
    } else {
      res.status(401).json({ message: 'Email ou senha incorretos.' });
    }
  }

  static async getAllClientes(req, res) {
    const query = `select * from cliente`;

    try {
      connect.query(query, function (err, data) {
        if (err) {
          console.log(err);
          res
            .status(500)
            .json({ error: "Usuários não encontrados no banco de dados!" });
          return;
        } //Fim do if
        let clientes = data;
        console.log("Consulta realizada com sucesso!");
        res.status(201).json({ clientes });
      }); //Fim do connect query
    } catch (error) {
      console.error("Erro ao executar a consulta: ", error);
      res.status(500).json({ error: "Erro interno do servidor!" });
    } // Fim do catch
  } // Fim do getAllClientes

static async getAllClientes2(req, res) {
    //metódo para selecionar clientes via parâmetros específicos

    //extrair parâmetros da consulta URL
    const { filtro, ordenacao } = req.query;

    //construir a consulta SQL base
    let query = `select * from cliente`;

    //adicionar a cláusula where, quando houver
    if (filtro) {
      //query = query + filtro; - incorreto
      //query = query + ` where ${filtro}`;
      query += ` where ${filtro}`;
    }

    try {
      connect.query(query, function (err, result) {
        if (err) {
          console.log(err);
          res
            .status(500)
            .json({ error: "Usuários não encontrados no banco de dados!" });
          return;
        } //Fim do if

        console.log("Consulta realizada com sucesso!");
        res.status(201).json({ result });
      }); //Fim do connect query
    } catch (error) {
      console.error("Erro ao executar a consulta: ", error);
      res.status(500).json({ error: "Erro interno do servidor!" });
    } // Fim do catch
  } // Fechamento do getAllClientes2
};
