const { connection } = require('../db/connect');

module.exports = class VooController {
    static async criarVoo(req, res) {
        const { Numero, CompanhiaAerea, Origem, Destino, Data, Hora, Classe, Preco, AssentosDisponiveis } = req.body;

        try {
            const query = 'INSERT INTO Voo (Numero, CompanhiaAerea, Origem, Destino, Data, Hora, Classe, Preco, AssentosDisponiveis) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)';
            await connection.query(query, [Numero, CompanhiaAerea, Origem, Destino, Data, Hora, Classe, Preco, AssentosDisponiveis]);
            res.status(201).json({ message: 'Voo criado com sucesso.' });
        } catch (error) {
            console.error('Erro ao criar voo:', error);
            res.status(500).json({ message: 'Erro ao criar voo.' });
        }
    }

    static async listarVoos(req, res) {
        try {
            const query = 'SELECT * FROM Voo';
            const [rows] = await connection.query(query);
            res.status(200).json({ voos: rows });
        } catch (error) {
            console.error('Erro ao listar voos:', error);
            res.status(500).json({ message: 'Erro ao listar voos.' });
        }
    }

    static async atualizarVoo(req, res) {
        const { Numero } = req.params;
        const { CompanhiaAerea, Origem, Destino, Data, Hora, Classe, Preco, AssentosDisponiveis } = req.body;

        try {
            const query = 'UPDATE Voo SET CompanhiaAerea=?, Origem=?, Destino=?, Data=?, Hora=?, Classe=?, Preco=?, AssentosDisponiveis=? WHERE Numero=?';
            await connection.query(query, [CompanhiaAerea, Origem, Destino, Data, Hora, Classe, Preco, AssentosDisponiveis, Numero]);
            res.status(200).json({ message: 'Voo atualizado com sucesso.' });
        } catch (error) {
            console.error('Erro ao atualizar voo:', error);
            res.status(500).json({ message: 'Erro ao atualizar voo.' });
        }
    }

    static async deletarVoo(req, res) {
        const { Numero } = req.params;

        try {
            const query = 'DELETE FROM Voo WHERE Numero=?';
            await connection.query(query, [Numero]);
            res.status(200).json({ message: 'Voo deletado com sucesso.' });
        } catch (error) {
            console.error('Erro ao deletar voo:', error);
            res.status(500).json({ message: 'Erro ao deletar voo.' });
        }
    }
};
