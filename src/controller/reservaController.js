const { connection } = require('../db/connect');

module.exports = class ReservaController {
    static async criarReserva(req, res) {
        const { Cliente_ID, Voo_Numero, AssentosReservados, ValorTotal } = req.body;

        try {
            const query = 'INSERT INTO Reserva (Cliente_ID, Voo_Numero, AssentosReservados, ValorTotal) VALUES (?, ?, ?, ?)';
            await connection.query(query, [Cliente_ID, Voo_Numero, AssentosReservados, ValorTotal]);
            res.status(201).json({ message: 'Reserva criada com sucesso.' });
        } catch (error) {
            console.error('Erro ao criar reserva:', error);
            res.status(500).json({ message: 'Erro ao criar reserva.' });
        }
    }

    static async listarReservas(req, res) {
        try {
            const query = 'SELECT * FROM Reserva';
            const [rows] = await connection.query(query);
            res.status(200).json({ reservas: rows });
        } catch (error) {
            console.error('Erro ao listar reservas:', error);
            res.status(500).json({ message: 'Erro ao listar reservas.' });
        }
    }

    static async atualizarReserva(req, res) {
        const { Reserva_ID } = req.params;
        const { Cliente_ID, Voo_Numero, AssentosReservados, ValorTotal } = req.body;

        try {
            const query = 'UPDATE Reserva SET Cliente_ID=?, Voo_Numero=?, AssentosReservados=?, ValorTotal=? WHERE Reserva_ID=?';
            await connection.query(query, [Cliente_ID, Voo_Numero, AssentosReservados, ValorTotal, Reserva_ID]);
            res.status(200).json({ message: 'Reserva atualizada com sucesso.' });
        } catch (error) {
            console.error('Erro ao atualizar reserva:', error);
            res.status(500).json({ message: 'Erro ao atualizar reserva.' });
        }
    }

    static async deletarReserva(req, res) {
        const { Reserva_ID } = req.params;

        try {
            const query = 'DELETE FROM Reserva WHERE Reserva_ID=?';
            await connection.query(query, [Reserva_ID]);
            res.status(200).json({ message: 'Reserva deletada com sucesso.' });
        } catch (error) {
            console.error('Erro ao deletar reserva:', error);
            res.status(500).json({ message: 'Erro ao deletar reserva.' });
        }
    }
};
